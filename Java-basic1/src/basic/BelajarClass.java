package basic;

public class BelajarClass {
    String name;
    String address;

    public String getName(){
        return name;
    }

    public void setName(String name) {
        System.out.println("Hello");
        this.name = name;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public static void main(String[] args) {
        BelajarClass belajarClass = new BelajarClass();
        belajarClass.setName("adam");
        System.out.println(belajarClass.getName());
        belajarClass.setAddress("Bdg");
        System.out.println(belajarClass.address);
    }
}
